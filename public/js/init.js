$(document).ready(function(){
    $('.header_brands').click(function(){
        var brand_id = $(this).data('brand_id');
        var url = $(this).data('url');
        var _token = $(this).data('token');
        $.ajax({
            url: url,
            type: 'POST',
            data: {_token:_token,brand_id:brand_id},
            success: function (data) {
                window.location.replace('/');
            }
        });
    })

    $("#login_form").submit(function (e) {
        e.preventDefault();

        var formData = $("#login_form").serialize();

        $.ajax({
            url: '/login',
            type: 'POST',
            data: formData,
            success: function (data) {
                if(data.success == true){
                    window.location.replace(data.url);
                }
            },
            error: function (data) {
                $(document).find('.error_login').text(JSON.parse(data.responseText).error);
            }
        });
    });

    /**
     * Register form ajax request
     */

    $("#register_form").submit(function (e) {
        e.preventDefault();

        var formData = $("#register_form").serialize();

        $.ajax({
            url: '/register',
            type: 'POST',
            data: formData,
            success: function (data) {
                window.location.reload();
            },
            error: function (data) {
                $.each( data.responseJSON, function( key, val ) {
                    var parent = $('#register_form').find("input[name='"+key+"']").parents('.input_field').addClass('has-error');
                    var html = '<span class="help-block">\n\
                                <strong>'+val[0]+'</strong>\n\
                            </span>';
                    parent.append(html);
                })
            }
        });
    });

});