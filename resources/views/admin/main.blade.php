@include('admin.layouts._header')

@yield('style')

<div id="wrapper">
    @include('admin.layouts._side_bar')
    <div id="page-wrapper" class="gray-bg dashbard-1">

        @include('admin.layouts._top_nav')
        <div style="margin-top: 100px">
            @yield('content')
        </div>
    </div>

</div>

@yield('script')

@include('admin.layouts._footer')