@extends('admin.main')

@section('content')
    <h1>Edit Brand</h1>
    <div class="container">
        @if(isset($brand) && !empty($brand))
            <div class="row">
                <div class="col-md-4">
                    <form action="{{url('/admin/brands/update/'.$brand->id)}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="brand_name">Brand Name:</label>
                            <input type="text" class="form-control" value="{{$brand->name}} {{ old('brand_name') }}" name="brand_name" id="brand_name">
                            @if($errors->has('brand_name'))
                                <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('brand_name')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <h4>Current Logo</h4>
                            <img src="{{'/'.$brand->logo}}" width="100px" alt="">
                        </div>
                        <label><input type="file" name="file">Add Logo for Brand</label>

                        <button type="submit" class="btn btn-info">Edit Brand</button>
                    </form>
                </div>
            </div>
        @endif
    </div>

@endsection