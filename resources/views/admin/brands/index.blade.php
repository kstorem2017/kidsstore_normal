@extends('admin.main')

@section('content')

    <h2>All Brands</h2>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th>Brand Name</th>
            <th>Logo</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($data) && !empty($data))
            @foreach($data as $brand)
                <tr>
                    <td>{{$brand->name}}</td>
                    <td><img src="/{{$brand->logo}}" width="100px" alt=""></td>
                    <td>
                        <a href="{{url('/admin/brands/edit/'.$brand->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                        <form action="{{url('/admin/brands/destroy/'.$brand->id)}}" method="post" style="display: inline-block" >
                            {{csrf_field()}}
                            <button type="submit" disabled href="{{url('/admin/destroy/edit/'.$brand->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection