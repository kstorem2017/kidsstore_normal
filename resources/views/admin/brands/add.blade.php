@extends('admin.main')

@section('content')
    <h1>Add New Brand</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="{{url('/admin/brands/store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="brand_name">Brand Name:</label>
                        <input type="text" class="form-control" value="{{ old('brand_name') }}" name="brand_name" id="brand_name">
                        @if($errors->has('brand_name'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('brand_name')}}</p>
                        @endif
                    </div>
                        <label><input type="file" name="file"> Logo for Brand</label>
                    <button type="submit" class="btn btn-info">Add Brand</button>
                </form>
            </div>
        </div>
    </div>
@endsection