@extends('admin.main')

@section('content')
    @if(isset($brands) && !empty($brands))
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter Categories By Brands
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="{{url('/admin/category/?brand=')}}">All</a></li>
            @foreach($brands as $brand)
                <li><a href="{{url('/admin/category/?brand='.$brand->id)}}">{{$brand->name}}</a></li>
            @endforeach
            </ul>
        </div>
    @endif

    <h2>All Categories</h2>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th>Category Name</th>
            <th>Brand Name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($data) && !empty($data))
            @foreach($data as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td>{{$category->brand_name}}</td>
                    <td>
                        <a href="" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="{{url('/admin/category/edit/'.$category->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('/admin/category/delete/'.$category->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection