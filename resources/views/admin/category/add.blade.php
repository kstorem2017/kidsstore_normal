@extends('admin.main')

@section('content')
    <h1>Add New Category</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="{{url('/admin/category/store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Category Name:</label>
                        <input type="text" class="form-control" value="{{ old('name') }}" name="name" id="name">
                        @if($errors->has('name'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('name')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        @if(isset($brands) && !empty($brands))
                            @foreach($brands as $brand)
                                <div class="radio">
                                    <label><input type="radio" name="brand" value="{{$brand->id}}">{{$brand->name}}</label>
                                </div>
                            @endforeach
                        @endif
                        @if($errors->has('brand'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('brand')}}</p>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-info">Add Category</button>
                </form>
            </div>
        </div>
    </div>
@endsection