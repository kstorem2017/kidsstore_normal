@extends('admin.main')

@section('content')

    <h1>Add New Product</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="{{url('/admin/product/store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Product Name:</label>
                        <input type="text" class="form-control" value="{{ old('name') }}" name="name" id="name">
                        @if($errors->has('name'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('name')}}</p>
                        @endif
                    </div>
                    <div class="form-group brands" data-token="{{csrf_token()}}">
                        <h3>Select Brand</h3>
                        @if(isset($brands) && !empty($brands))
                            @foreach($brands as $brand)
                                <div class="radio">
                                    <label><input type="radio" class="select_brand" name="brand" data-action="{{url('/admin/category/getByBrandId/'.$brand->id)}}" value="{{$brand->id}}">{{$brand->name}}</label>
                                </div>
                            @endforeach
                        @endif
                        @if($errors->has('brand'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('brand')}}</p>
                        @endif
                    </div>
                    <div class="form-group categories_by_brand" data-action="{{url('/admin/subcategory/getByCategoryId')}}"  data-token="{{csrf_token()}}">

                    </div>
                    <div class="form-group subcategories_by_category">

                    </div>
                    <div class="form-group">
                        <h3>Select Sizes</h3>
                        @if(isset($sizes) && !empty($sizes))
                            @foreach($sizes as $size)
                                <div class="checkbox">
                                    <label><input type="checkbox" value="{{$size->id}}" name="sizes[]"/>{{$size->size}}</label>
                                </div>
                            @endforeach
                        @endif
                        @if($errors->has('sizes'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('sizes')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea class="form-control" rows="5" id="description" name="description">{{ old('description') }}</textarea>
                    </div>
                    @if($errors->has('description'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('description')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="price">Price AMD:</label>
                        <input type="number" class="form-control" value="{{old('price')}}" name="price" id="price">
                    </div>
                    @if($errors->has('price'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('price')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="discount">Discount %:</label>
                        <input type="number" class="form-control" value="{{old('discount')}}" name="discount" id="discount">
                    </div>
                    @if($errors->has('discount'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('discount')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="discount_time">Discount Till:</label>
                        <input type="date" class="form-control" value="{{old('discount_time')}}" name="discount_time" id="discount_time">
                    </div>
                    @if($errors->has('discount_time'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('discount_time')}}</p>
                    @endif
                    <div class="checkbox">
                        <label><input type="checkbox" value="" name="exist">Exist</label>
                    </div>
                    @if($errors->has('exist'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('exist')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="original_url">Original Url:</label>
                        <input type="text" class="form-control" value="{{old('original_url')}}" name="original_url" id="original_url">
                    </div>
                    @if($errors->has('original_url'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('original_url')}}</p>
                    @endif
                    <div class="form-group">
                        <input type="file" name="images[]" multiple>
                    </div>
                    @if($errors->has('images'))
                        <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('images')}}</p>
                    @endif
                    <button type="submit" class="btn btn-info">Add Product</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click','.select_brand',function(){
            var url = $(this).data('action');
            var token = $(this).parents('.form-group').data('token');
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                data:{_token:token},
                success:function(result){
                    $('.categories_by_brand').empty();
                    $('.subcategories_by_category').empty();
                    var html = '<h3>Select Category</h3>';
                    $.each( result, function( key, value ) {
                        html += '<div class="radio"><label><input type="radio" class="select_category" name="category" value="'+value.id+'">'+value.name+'</label></div>';
                    });
                    $('.categories_by_brand').append(html);
                }
            })
        })

        $(document).on('click','.select_category',function(){
            var url = $(this).parents('.form-group').data('action');
            url = url + '/' + $(this).val();
            var token = $(this).parents('.form-group').data('token');
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                data:{_token:token},
                success:function(result){
                    $('.subcategories_by_category').empty();
                    var html = '<h3>Select Subcategory</h3>';
                    $.each( result, function( key, value ) {
                        html += '<div class="radio"><label><input type="radio" class="select_subcategory" name="subcategory" value="'+value.id+'">'+value.name+'</label></div>';
                    });
                    $('.subcategories_by_category').append(html);
                }
            })
        })
    </script>
@endsection