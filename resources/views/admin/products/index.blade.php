@extends('admin.main')

@section('content')

    <h1>All Products</h1>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Subcategory</th>
            <th>Description</th>
            <th>Price</th>
            <th>Discount</th>
            <th>Discount time</th>
            <th>Exists</th>
            <th>Code</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($products) && !empty($products))
            @foreach($products as $product)
                <tr style="background: {{(!empty($product->deleted_at) ? 'rgba(215, 44, 44, 0.5)' : '')}}">
                    <td>{{$product->name}}</td>
                    <td>
                        @foreach($brands as $brand)
                            @if($brand->id == $product->brand_id)
                                {{$brand->name}}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach($categories as $category)
                            @if($category->id == $product->category_id)
                                {{$category->name}}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach($subcategories as $subcategory)
                            @if($subcategory->id == $product->subcategory_id)
                                {{$subcategory->name}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->discount}}</td>
                    <td>{{$product->discount_time}}</td>
                    <td>
                        @if($product->exist == 1)
                            Yes
                        @elseif($product->exist == 0)
                            No
                        @endif
                    </td>
                    <td>
                        {{$product->hash}}
                    </td>
                    <td>
                        <a href="{{url('/admin/product/show/'.$product->id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        @if(empty($product->deleted_at))
                            <a href="{{url('/admin/product/delete/'.$product->id)}}" title="delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        @else
                            <a href="{{url('/admin/product/restore/'.$product->id)}}" title="restore" class="btn btn-danger"><i class="fa fa-refresh"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    {{ $products->links() }}
@endsection

@section('script')

@endsection