<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>KidsStore | Dashboard</title>

    <link href="{!! asset('css/admin/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('font-awesome/css/font-awesome.css') !!}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{!! asset('css/admin/plugins/toastr/toastr.min.css') !!}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{!! asset('js/admin/plugins/gritter/jquery.gritter.css') !!}" rel="stylesheet">

    <link href="{!! asset('css/admin/animate.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/admin/style.css') !!}" rel="stylesheet">

    <script src="{!! asset('js/admin/jquery-3.1.1.min.js') !!}"></script>
    <script src="{!! asset('js/admin/bootstrap.min.js') !!}"></script>

</head>

<body>