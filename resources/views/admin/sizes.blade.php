@extends('admin.main')

@section('content')
    <div class="text-center">
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#add_size_modal">Add New Size</a>
    </div>
    <h2>All Sizes</h2>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Size</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($data) && !empty($data))
            @foreach($data as $size)
                <tr>
                    <td>{{$size->id}}</td>
                    <td>{{$size->size}}</td>
                    <td>
                        <a href="{{url('admin/sizes/edit/'.$size->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/sizes/destroy/'.$size->id)}}" data-token="{{csrf_token()}}" class="btn btn-danger delete_size"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>


    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div id="add_size_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Size</h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('/admin/sizes/store')}}" method="post" id="add_size_form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="size">Size:</label>
                            <input type="text" class="form-control" name="size" id="size">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('submit','#add_size_form',function(e){
            e.preventDefault()
            var form_data = $(this).serialize();
            var url = $(this).attr('action');
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                data:form_data,
                success:function(result){
                    if(result.success == true){
                        location.reload();
                    }
                },
                error:function(result){
                    $.each( result.responseJSON, function( key, val ) {
                        var parent = $('#add_size_form').find("input[name='"+key+"']").parents('.form-group').addClass('has-error');
                        var html = '<span class="help-block">\n\
                                <strong>'+val[0]+'</strong>\n\
                            </span>';
                        parent.append(html);
                    })
                }
            })
        })

        $(document).on('click','.delete_size',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var token = $(this).data('token')
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                data:{_token:token},
                success:function(result){
                    if(result.success == true){
                        location.reload();
                    }
                }
            })
        })
    </script>
@endsection