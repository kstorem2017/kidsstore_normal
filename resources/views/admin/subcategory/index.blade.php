@extends('admin.main')

@section('content')
    @if(isset($categories) && !empty($categories))
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter Subcategories By Category
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="{{url('/admin/subcategory/?category=')}}">All</a></li>
                @foreach($categories as $category)
                    <li><a href="{{url('/admin/subcategory/?category='.$category->id)}}">{{$category->name}} ({{$category->brand_name}})</a></li>
                @endforeach
            </ul>
        </div>
    @endif

    <h2>All Subcategories</h2>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th>Subcategory Name</th>
            <th>Category Name</th>
            <th>Brand Name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($subcategories) && !empty($subcategories))
            @foreach($subcategories as $subcategory)
                <tr>
                    <td>{{$subcategory->name}}</td>
                    <td>{{$subcategory->category_name}}</td>
                    <td>{{$subcategory->brand_name}}</td>
                    <td>
                        <a href="" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection