@extends('admin.main')

@section('content')
    <h1>Add New Subcategory</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="{{url('/admin/subcategory/store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Subcategory Name:</label>
                        <input type="text" class="form-control" value="{{ old('name') }}" name="name" id="name">
                        @if($errors->has('name'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('name')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        @if(isset($categories) && !empty($categories))
                            @foreach($categories as $category)
                                <div class="radio">
                                    <label><input type="radio" name="category" value="{{$category->id}}">{{$category->name}} ({{$category->brand_name}})</label>
                                </div>
                            @endforeach
                        @endif
                        @if($errors->has('brand'))
                            <p style="color:red;font-size: 12px;text-transform: capitalize">{{$errors->first('brand')}}</p>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-info">Add Subcategory</button>
                </form>
            </div>
        </div>
    </div>
@endsection