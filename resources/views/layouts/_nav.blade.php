
<div class="header_bottom">
    <div class="container">
        <div class="col-xs-8 header-bottom-left">
            <div class="col-xs-2 logo">
                <h1><a href="{{url('/')}}"><span>Kids</span>Store</a></h1>
            </div>
            <div class="col-xs-6 menu">
                <ul class="megamenu skyblue">
                    @if(isset($categories) && !empty($categories))
                        @foreach($categories as $category)
                            <li>
                                <a class="color6" href="{{url('/category/'.$category->id)}}">{{$category->name}}</a>
                                @if(isset($category->subcategories) && !empty($category->subcategories))
                                    <div class="megapanel">
                                        <div class="row">

                                            <div class="col1">
                                                <div class="h_nav">
                                                    <ul>
                                                        @foreach($category->subcategories as $subcategory)
                                                            <li><a href="{{url('/subcategory/'.$category->id.'/'.$subcategory->id)}}">{{$subcategory->name}}</a></li>
                                                        @endforeach
                                                        {{--<li><a href="men.html">Bags</a></li>--}}
                                                        {{--<li><a href="men.html">Caps & Hats</a></li>--}}
                                                        {{--<li><a href="men.html">Hoodies & Sweatshirts</a></li>--}}
                                                        {{--<li><a href="men.html">Jackets & Coats</a></li>--}}
                                                        {{--<li><a href="men.html">Jeans</a></li>--}}
                                                        {{--<li><a href="men.html">Jewellery</a></li>--}}
                                                        {{--<li><a href="men.html">Jumpers & Cardigans</a></li>--}}
                                                        {{--<li><a href="men.html">Leather Jackets</a></li>--}}
                                                        {{--<li><a href="men.html">Long Sleeve T-Shirts</a></li>--}}
                                                        {{--<li><a href="men.html">Loungewear</a></li>--}}
                                                    </ul>
                                                </div>
                                            </div>
                                            {{--<div class="col1">--}}
                                                {{--<div class="h_nav">--}}
                                                    {{--<ul>--}}
                                                        {{--<li><a href="men.html">Shirts</a></li>--}}
                                                        {{--<li><a href="men.html">Shoes, Boots & Trainers</a></li>--}}
                                                        {{--<li><a href="men.html">Shorts</a></li>--}}
                                                        {{--<li><a href="men.html">Suits & Blazers</a></li>--}}
                                                        {{--<li><a href="men.html">Sunglasses</a></li>--}}
                                                        {{--<li><a href="men.html">Sweatpants</a></li>--}}
                                                        {{--<li><a href="men.html">Swimwear</a></li>--}}
                                                        {{--<li><a href="men.html">Trousers & Chinos</a></li>--}}
                                                        {{--<li><a href="men.html">T-Shirts</a></li>--}}
                                                        {{--<li><a href="men.html">Underwear & Socks</a></li>--}}
                                                        {{--<li><a href="men.html">Vests</a></li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col1">--}}
                                                {{--<div class="h_nav">--}}
                                                    {{--<h4>Popular Brands</h4>--}}
                                                    {{--<ul>--}}
                                                        {{--<li><a href="men.html">Levis</a></li>--}}
                                                        {{--<li><a href="men.html">Persol</a></li>--}}
                                                        {{--<li><a href="men.html">Nike</a></li>--}}
                                                        {{--<li><a href="men.html">Edwin</a></li>--}}
                                                        {{--<li><a href="men.html">New Balance</a></li>--}}
                                                        {{--<li><a href="men.html">Jack & Jones</a></li>--}}
                                                        {{--<li><a href="men.html">Paul Smith</a></li>--}}
                                                        {{--<li><a href="men.html">Ray-Ban</a></li>--}}
                                                        {{--<li><a href="men.html">Wood Wood</a></li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    @endif
                    {{--<li class="active grid"><a class="color1" href="index.html">Men</a>--}}
                        {{--<div class="megapanel">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Accessories</a></li>--}}
                                            {{--<li><a href="men.html">Bags</a></li>--}}
                                            {{--<li><a href="men.html">Caps & Hats</a></li>--}}
                                            {{--<li><a href="men.html">Hoodies & Sweatshirts</a></li>--}}
                                            {{--<li><a href="men.html">Jackets & Coats</a></li>--}}
                                            {{--<li><a href="men.html">Jeans</a></li>--}}
                                            {{--<li><a href="men.html">Jewellery</a></li>--}}
                                            {{--<li><a href="men.html">Jumpers & Cardigans</a></li>--}}
                                            {{--<li><a href="men.html">Leather Jackets</a></li>--}}
                                            {{--<li><a href="men.html">Long Sleeve T-Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Loungewear</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Shoes, Boots & Trainers</a></li>--}}
                                            {{--<li><a href="men.html">Shorts</a></li>--}}
                                            {{--<li><a href="men.html">Suits & Blazers</a></li>--}}
                                            {{--<li><a href="men.html">Sunglasses</a></li>--}}
                                            {{--<li><a href="men.html">Sweatpants</a></li>--}}
                                            {{--<li><a href="men.html">Swimwear</a></li>--}}
                                            {{--<li><a href="men.html">Trousers & Chinos</a></li>--}}
                                            {{--<li><a href="men.html">T-Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Underwear & Socks</a></li>--}}
                                            {{--<li><a href="men.html">Vests</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<h4>Popular Brands</h4>--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Levis</a></li>--}}
                                            {{--<li><a href="men.html">Persol</a></li>--}}
                                            {{--<li><a href="men.html">Nike</a></li>--}}
                                            {{--<li><a href="men.html">Edwin</a></li>--}}
                                            {{--<li><a href="men.html">New Balance</a></li>--}}
                                            {{--<li><a href="men.html">Jack & Jones</a></li>--}}
                                            {{--<li><a href="men.html">Paul Smith</a></li>--}}
                                            {{--<li><a href="men.html">Ray-Ban</a></li>--}}
                                            {{--<li><a href="men.html">Wood Wood</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="grid"><a class="color2" href="#">Women</a>--}}
                        {{--<div class="megapanel">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Accessories</a></li>--}}
                                            {{--<li><a href="men.html">Bags</a></li>--}}
                                            {{--<li><a href="men.html">Caps & Hats</a></li>--}}
                                            {{--<li><a href="men.html">Hoodies & Sweatshirts</a></li>--}}
                                            {{--<li><a href="men.html">Jackets & Coats</a></li>--}}
                                            {{--<li><a href="men.html">Jeans</a></li>--}}
                                            {{--<li><a href="men.html">Jewellery</a></li>--}}
                                            {{--<li><a href="men.html">Jumpers & Cardigans</a></li>--}}
                                            {{--<li><a href="men.html">Leather Jackets</a></li>--}}
                                            {{--<li><a href="men.html">Long Sleeve T-Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Loungewear</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Shoes, Boots & Trainers</a></li>--}}
                                            {{--<li><a href="men.html">Shorts</a></li>--}}
                                            {{--<li><a href="men.html">Suits & Blazers</a></li>--}}
                                            {{--<li><a href="men.html">Sunglasses</a></li>--}}
                                            {{--<li><a href="men.html">Sweatpants</a></li>--}}
                                            {{--<li><a href="men.html">Swimwear</a></li>--}}
                                            {{--<li><a href="men.html">Trousers & Chinos</a></li>--}}
                                            {{--<li><a href="men.html">T-Shirts</a></li>--}}
                                            {{--<li><a href="men.html">Underwear & Socks</a></li>--}}
                                            {{--<li><a href="men.html">Vests</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col1">--}}
                                    {{--<div class="h_nav">--}}
                                        {{--<h4>Popular Brands</h4>--}}
                                        {{--<ul>--}}
                                            {{--<li><a href="men.html">Levis</a></li>--}}
                                            {{--<li><a href="men.html">Persol</a></li>--}}
                                            {{--<li><a href="men.html">Nike</a></li>--}}
                                            {{--<li><a href="men.html">Edwin</a></li>--}}
                                            {{--<li><a href="men.html">New Balance</a></li>--}}
                                            {{--<li><a href="men.html">Jack & Jones</a></li>--}}
                                            {{--<li><a href="men.html">Paul Smith</a></li>--}}
                                            {{--<li><a href="men.html">Ray-Ban</a></li>--}}
                                            {{--<li><a href="men.html">Wood Wood</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li><a class="color4" href="about.html">About</a></li>--}}
                    {{--<li><a class="color5" href="404.html">Blog</a></li>--}}
                    {{--<li><a class="color6" href="contact.html">Support</a></li>--}}
                </ul>
            </div>
        </div>
        <div class="col-xs-4 header-bottom-right">
            <div class="box_1-cart">
                <div class="box_11">
                    {{--<a href="checkout.html">--}}
                    <a href="javascript:void (0)">
                        <h4><p>Cart: <span class="simpleCart_total">9000</span> AMD (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</p><img src="images/bag.png" alt=""/><div class="clearfix"> </div></h4>
                    </a>
                    <div class="cart_dropdown">
                      <table class="table">
                          <tr>
                              <td>
                                  <img  src="images/pic1.jpg" class="img-responsive"  alt="item4">
                              </td>
                              <td>
                                  <h2>humour</h2>
                                  <p>Non-charac</p>
                              </td>
                              <td>
                                  <span>$45.00</span>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <img  src="images/s1.jpg" class="img-responsive"  alt="item4">
                              </td>
                              <td>
                                  <h2>humour humour humour</h2>
                                  <p>Non-charac humour humour</p>
                              </td>
                              <td>
                                  <span>$45.00</span>
                              </td>
                          </tr>
                          <tfoot>
                             <tr>
                                 <td colspan="2">Total</td>
                                 <td>
                                     <span class="total_price">$45.00</span>
                                 </td>
                             </tr>
                          </tfoot>
                      </table>
                    </div>
                </div>
                <p class="empty"><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
                <div class="clearfix"> </div>
            </div>
            {{--<div class="search">--}}
                {{--<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">--}}
                {{--<input type="submit" value="Subscribe" id="submit" name="submit">--}}
                {{--<div id="response"> </div>--}}
            {{--</div>--}}
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>