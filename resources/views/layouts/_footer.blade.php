
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="col-md-4 box_3">
                <h3>Our Stores</h3>
                <address class="address">
                    <p>9870 St Vincent Place, <br>Glasgow, DC 45 Fr 45.</p>
                    <dl>
                        <dt></dt>
                        <dd>Freephone:<span> +1 800 254 2478</span></dd>
                        <dd>Telephone:<span> +1 800 547 5478</span></dd>
                        <dd>FAX: <span>+1 800 658 5784</span></dd>
                        <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd>
                    </dl>
                </address>
                <ul class="footer_social">
                    <li><a href=""> <i class="fb"> </i> </a></li>
                    <li><a href=""><i class="tw"> </i> </a></li>
                    <li><a href=""><i class="google"> </i> </a></li>
                    <li><a href=""><i class="instagram"> </i> </a></li>
                </ul>
            </div>
            <div class="col-md-4 box_3">
                <h3>Blog Posts</h3>
                <h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
                <h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
                <h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
            </div>
            <div class="col-md-4 box_3">
                <h3>Support</h3>
                <ul class="list_1">
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Payment</a></li>
                    <li><a href="#">Refunds</a></li>
                    <li><a href="#">Track Order</a></li>
                    <li><a href="#">Services</a></li>
                </ul>
                <ul class="list_1">
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Press</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="footer_bottom">
            <div class="copy">
                <p>Copyright © {{date("Y")}} KidsStore.am. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="login_signup_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div style="width: 100%;float: left">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3>Sign Up</h3>

                        <form id="register_form" method="POST" action="{{ url('/register') }}">
                            {{csrf_field()}}
                            <div class="user_name input_field">
                                <input type="text" placeholder="Email" name="username" class="form-control">
                            </div>
                            <div class="user_email input_field">
                                <input type="email" placeholder="Email Address" name="email" class="form-control">
                            </div>

                            <div class="user_password input_field">
                              <input type="password" name="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="user_password input_field">
                                 <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
                             </div>
                            {{--<div class="input_field">--}}
                               {{--<input type="checkbox" id="terms" name="terms_conditions">--}}
                                {{--<label for="terms"> I accept the terms & conditions </label>--}}
                            {{--</div>--}}
                            <button type="submit" class="btn">Sign Up</button>
                        </form>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3>Sign In</h3>
                        <form id="login_form">
                            {{csrf_field()}}
                            <input type="hidden" name="current_url" value="{{Request::fullUrl()}}">
                            <div class="user_email input_field">
                                <input type="text" placeholder="Username or Email Address" name="email" class="form-control">
                            </div>
                            <div class="user_password input_field">
                              <input type="password" name="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="input_field">
                                <a href="{{url('password/reset')}}"> Forgot Password?</a>
                            </div>
                            <p class="error_login" style="color: red"></p>
                            <button type="submit" class="btn">Sign In</button>
                        </form>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/init.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.home_slider').slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            autoplay:true,
            arrows: true,
            autoplaySpeed: 4000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: false,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]

    });
    });
    $('.header-bottom-right .box_11').on('click',function(){
        $('.cart_dropdown').slideToggle();
    });
</script>

</body>
</html>