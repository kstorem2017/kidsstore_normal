<!DOCTYPE HTML>
<html>
<head>
    <title>Buy_shop an E-Commerce online Shopping Category Flat Bootstarp responsive Website Template| Home :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Buy_shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Custom Theme files -->
    <link href="css/slick.css" rel='stylesheet' type='text/css' />
    <link href="{{asset('/font-awesome/css/font-awesome.css')}}" rel='stylesheet' type='text/css' />
    <link href="css/slick-theme.css" rel='stylesheet' type='text/css' />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/new_style.css" rel='stylesheet' type='text/css' />
    {{--<script src="js/simpleCart.min.js"> </script>--}}
    <!-- Custom Theme files -->
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <!-- start menu -->
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="js/megamenu.js"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
</head>
<body>
<div class="header_top">
    <div class="container">
        <div class="one-fifth column row_1">
			<span class="selection-box">
                @if(isset($brands) && !empty($brands))
                    @foreach($brands as $brand)
                        <?php
                            $current_brand = false;
                            if($_SESSION['brand_id'] == $brand->id){
                                $current_brand = true;
                            }
                        ?>
                        <span class="header_brands pull-left" data-token="{{csrf_token()}}" data-url="{{url('/change-brand')}}" data-brand_id="{{$brand->id}}" title="{{$brand->name}}" style="background-image: url('{{$brand->logo}}');">
                            @if($current_brand)
                                <span class="active_brand"></span>
                            @endif
                        </span>
                    @endforeach
                @endif
            </span>
        </div>
        <div class="cssmenu">
            <ul>
                <li class="active">
                    {{--<a href="login.html">My Account</a>--}}
                    @if (!Auth::guest())
                    <div class="dropdown">
                        <a class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            {{Auth::user()->name}}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">My Orders</a></li>
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            <li role="separator" class="divider"></li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                        </ul>
                    </div>
                    @endif

                </li>
                @if (Auth::guest())
                    <a href="{{url('/auth/facebook')}}">
                        <img src="/img/LKMP7.png" alt="">
                    </a>
                    <li><a href="javascript:void(0)" class="btn btn-default" data-toggle="modal" data-target="#login_signup_popup">Log In</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="wrap-box"></div>

@include('layouts._nav')