@include('layouts._header')

    @yield('style')

    @yield('content')

    @yield('script')

@include('layouts._footer')