<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Subcategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $category_filter = Input::get('category');
        if(empty($category_filter)){
            $data['subcategories'] = Subcategories::getSubcategoriesWithCatsAndBrands();

        }
        else{
            $data['subcategories'] = Subcategories::getSubcategoriesWithCatsAndBrandsFilter($category_filter);

        }
        $data['categories'] = Categories::getCategoriesWithBrands();
//        dd($data);
        return view('admin.subcategory.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $categories = Categories::getCategoriesWithBrands();
        return view('admin.subcategory.add')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'category' => 'required'
        ]);
//        dd($request->all());
        $subcategory = new Subcategories();
        $subcategory->name = $request->name;
        $subcategory->category_id = $request->category;
        $subcategory->save();
        return redirect(url('/admin/subcategory'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getByCategoryId($id){
        return Subcategories::where('category_id',$id)->get();
    }
}
