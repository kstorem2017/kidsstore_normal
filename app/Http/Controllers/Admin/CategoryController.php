<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brands;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand_filter = Input::get('brand');
        if(empty($brand_filter)){
            $data = Categories::getCategoriesWithBrands();
        }
        else{
            $data = Categories::getCategoriesWithBrandsFilter($brand_filter);
        }
        $brands = Brands::get();
        return view('admin.category.index',['data' => $data,'brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $brands = Brands::get();
        return view('admin.category.add')->withBrands($brands);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'brand' => 'required'
        ]);
//        dd($request->all());
        $category = new Categories();
        $category->name = $request->name;
        $category->brand_id = $request->brand;
        $category->save();
        return redirect(url('/admin/category'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['brands'] = Brands::get();
        $data['category'] = Categories::find($id);
        return view('admin.category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'brand' => 'required'
        ]);
        $category = Categories::find($id);
        $category->name = $request->name;
        $category->brand_id = $request->brand;
        $category->save();
        return redirect(url('/admin/category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }

    public function getByBrandId($id){
        return Categories::where('brand_id',$id)->get();
    }
}
