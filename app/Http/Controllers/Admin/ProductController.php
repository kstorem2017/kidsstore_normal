<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brands;
use App\Models\Categories;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductSizes;
use App\Models\Sizes;
use App\Models\Subcategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['brands'] = Brands::get();
        $data['categories'] = Categories::get();
        $data['subcategories'] = Subcategories::get();
        $data['all_sizes'] = Sizes::get();
        $data['products'] = Products::withTrashed()->with('sizes','images')->paginate(20);
//        dd($data);
        return view('admin.products.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data['sizes'] = Sizes::get();
        $data['brands'] = Brands::get();
        return view('admin.products.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $exist = 0;
        $hash = strtoupper(hash('adler32',$request->name.rand(0,99999999)));
//        dd($request->all());
        $this->validate($request,[
           'name' => 'required',
            'brand' => 'required',
            'category' => 'required',
            'subcategory' => 'required',
            'price' => 'required',
            'sizes' => 'required',
            'images' => 'required',
            'discount' => 'max:100'
        ]);


        if(isset($request->exist))
            $exist = 1;

        $product = new Products();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->hash = $hash;
        $product->brand_id = $request->brand;
        $product->category_id = $request->category;
        $product->subcategory_id = $request->subcategory;
        if(!empty($request->discount)){
            $product->discount = $request->discount;
        }
        if(!empty($request->discount_time)) {
            $product->discount_time = $request->discount_time;
        }
        $product->exist = $exist;
        $product->original_url = $request->original_url;
        $product->save();
        if(!empty($request->images)){
            foreach($request->images as $image){
                $this->uploadProductImage($product->id,$image);
            }
        }
        if(!empty($request->sizes)){
            foreach($request->sizes as $size){
                $size_obj = new ProductSizes();
                $size_obj->product_id = $product->id;
                $size_obj->size_id = $size;
                $size_obj->save();
            }
        }
        Products::where('id',$product->id)->update(['url' => url('/product/'.$product->id)]);
        return redirect('/admin/product');
    }

    public function uploadProductImage($prod_id,$file){
        $rule = ['file' => 'mimes:png,gif,jpeg,jpg'];
        $validator = Validator::make(array('images' => $file), $rule);
        if ($validator->passes()) {

//                $images_mod = new Images;
            $string = str_random(40);
            $destinationPath = 'uploads';
            $filename = 'img-' . $string . $file->getClientOriginalName();

            $file->move($destinationPath, $filename);
            $image_path = $destinationPath . '/' . $filename;
            $image = new ProductImages();
            $image->product_id = $prod_id;
            $image->img_path = $image_path;
            $image->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product['sizes'] = $this->getProductSizesById($id);
        $product['details'] = Products::withTrashed()->with('images')->where('id',$id)->first();
//        dd($product);
        return view('admin.products.show')->withProduct($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Products::find($id)->delete();
        return back();
    }

    public function restore($id){
        Products::withTrashed()
            ->where('id',$id)
            ->restore();
        return back();
    }

    public function getProductSizesById($id){
        return Products::select(
            'sizes.id',
            'sizes.size'
        )
        ->leftJoin('product_sizes', 'products.id', '=', 'product_sizes.product_id')
        ->leftJoin('sizes', 'product_sizes.size_id', '=', 'sizes.id')
        ->where('products.id',$id)
        ->get();
    }
}
