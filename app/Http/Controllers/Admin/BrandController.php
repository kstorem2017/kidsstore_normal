<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brands;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function index(){
        $data = Brands::get();
        return view('admin.brands.index')->withData($data);
    }

    public function add(){
        return view('admin.brands.add');

    }

    public function edit($id){
        $brand = Brands::find($id);
        return view('admin.brands.edit')->withBrand($brand);
    }

    public function update($brand_id,Request $request){
        $brand = Brands::find($brand_id);
        $brand->name = $request->brand_name;
        $brand->save();
        if(isset($request->file))
            $this->uploadBrandLogo($brand->id,$request->file);

        return redirect('/admin/brands');
    }

    public function store(Request $request){
        $this->validate($request,[
           'brand_name' => 'required'
        ]);

        $brand = new Brands();
        $brand->name = $request->brand_name;
        $brand->save();

        if(isset($request->file))
            $this->uploadBrandLogo($brand->id,$request->file);

        return redirect('/admin/brands');
    }

    public function uploadBrandLogo($brand_id,$file){
            $rule = ['file' => 'mimes:png,gif,jpeg,jpg'];
            $validator = Validator::make(array('images' => $file), $rule);
            if ($validator->passes()) {

//                $images_mod = new Images;
                $string = str_random(40);
                $destinationPath = 'images/logos';
                $filename = 'logo-' . $string . $file->getClientOriginalName();

                $file->move($destinationPath, $filename);
                $image_path = $destinationPath . '/' . $filename;
                $current_logo = Brands::where('id',$brand_id)->first();
                if (!empty(realpath($current_logo->logo))) {
                    unlink(realpath($current_logo->logo));
                }
                Brands::where('id',$brand_id)->update(['logo' => $image_path]);
            }
    }

    public function destroy($brand_id){
        $current_logo = Brands::where('id',$brand_id)->first();
        if (!empty(realpath($current_logo->logo))) {
            unlink(realpath($current_logo->logo));
        }
        Brands::where('id',$brand_id)->delete();
        return redirect('/admin/brands');
    }


}
