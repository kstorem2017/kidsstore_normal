<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use Illuminate\Http\Request;
use Session;
use App\Models\Products;
use App\Models\Subcategories;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function prods(){
        return view('prods');
    }

    public function changeBrandId(Request $request){
        if(empty($request->brand_id)){
            $this->getBrandID();
        }
        else{
            $_SESSION['brand_id'] = $request->brand_id;
        }
        $result['success'] = true;
        return $result;
    }

    public function product($id){
        dd($id);
    }

    public function categoryProducts($id){
        $data['subcategories'] = Subcategories::where('category_id',$id)->get();
        $data['products'] = Products::with('sizes','images')->where('category_id',$id)->paginate(20);
        dd($data);
    }

    public function subCategoryProducts($cat_id,$sub_cat_id){
        $data['subcategories'] = Subcategories::where('category_id',$cat_id)->get();
        $data['products'] = Products::with('sizes','images')->where('category_id',$cat_id)->where('subcategory_id',$sub_cat_id)->paginate(20);
        dd($data);
    }

}
