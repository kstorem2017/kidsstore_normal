<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Models\Brands;
use App\Models\Categories;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Session;
use DB;


class Controller extends BaseController
{
    public $brand_id = '';

    public function __construct(){
        $brand_id = $this->getBrandID();
        $data['brands'] = Brands::get();
        $data['categories'] = Categories::with(['subcategories'])->where(['brand_id'=>$brand_id])->get();
//        dd($data);
        View::share($data);
    }
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getBrandID(){
        session_start();
        if(isset($_SESSION['brand_id'])){
            $brand_id = $_SESSION['brand_id'];
        }
        else{
            $brand_id = $_SESSION['brand_id'] = '';
        }
//        dd($brand_id);
        if(empty($brand_id)){
            $first_brand = DB::table('brands')->where('order_id', DB::raw("(select min(`order_id`) from brands)"))->first();
            $brand_id = $_SESSION['brand_id'] = $first_brand->id;
        }
        return $brand_id;
    }
}
