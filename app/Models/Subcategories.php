<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subcategories extends Model
{
    public static function getSubcategoriesWithCatsAndBrands(){
        return DB::table('subcategories')
            ->select(
                'subcategories.id',
                'subcategories.name',
                'subcategories.category_id',
                'categories.name as category_name',
                'categories.brand_id',
                'brands.logo',
                'brands.name as brand_name'
            )
            ->leftJoin('categories', 'categories.id', '=', 'subcategories.category_id')
            ->leftJoin('brands', 'brands.id', '=', 'categories.brand_id')
            ->orderBy('subcategories.order_id')
            ->get();
    }

    public static function getSubcategoriesWithCatsAndBrandsFilter($filter){
        return DB::table('subcategories')
            ->select(
                'subcategories.id',
                'subcategories.name',
                'subcategories.category_id',
                'categories.name as category_name',
                'categories.brand_id',
                'brands.logo',
                'brands.name as brand_name'
            )
            ->leftJoin('categories', 'categories.id', '=', 'subcategories.category_id')
            ->leftJoin('brands', 'brands.id', '=', 'categories.brand_id')
            ->where('subcategories.category_id',$filter)
            ->orderBy('subcategories.order_id')
            ->get();
    }
}
