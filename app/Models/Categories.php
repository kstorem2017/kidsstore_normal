<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subcategories;
use DB;

class Categories extends Model
{
    public static function getCategoriesWithBrands(){
        return DB::table('categories')
            ->select(
                'categories.id',
                'categories.name',
                'categories.brand_id',
                'brands.name as brand_name',
                'brands.logo'
            )
            ->leftJoin('brands', 'brands.id', '=', 'categories.brand_id')
            ->orderBy('categories.order_id')
            ->get();
    }

    public static function getCategoriesWithBrandsFilter($filter){
        return DB::table('categories')
            ->select(
                'categories.id',
                'categories.name',
                'categories.brand_id',
                'brands.name as brand_name',
                'brands.logo'
            )
            ->leftJoin('brands', 'brands.id', '=', 'categories.brand_id')
            ->where('categories.brand_id',$filter)
            ->orderBy('categories.order_id')
            ->get();
    }

    public function subcategories(){
        return $this->hasMany('App\Models\Subcategories','category_id','id');
    }
}
