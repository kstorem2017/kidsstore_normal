<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
//    protected $table = 'products';
//
//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'id','name','description'
//    ];

    public function sizes(){
        return $this->hasMany('App\Models\ProductSizes','product_id','id');
    }

    public function images(){
        return $this->hasMany('App\Models\ProductImages','product_id','id');
    }

}
