<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::post('/change-brand','HomeController@changeBrandId');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index');


    /**
     * Brand Routes
     */

    Route::get('/brands', 'Admin\BrandController@index');
    Route::get('/brands/add', 'Admin\BrandController@add');
    Route::get('/brands/edit/{id}', 'Admin\BrandController@edit');
    Route::post('/brands/store', 'Admin\BrandController@store');
    Route::post('/brands/update/{id}', 'Admin\BrandController@update');
    Route::post('/brands/destroy/{id}', 'Admin\BrandController@destroy');

    /**
     * Category Routes
     */

    Route::get('/category', 'Admin\CategoryController@index');
    Route::get('/category/add', 'Admin\CategoryController@add');
    Route::post('/category/store', 'Admin\CategoryController@store');
    Route::get('/category/edit/{id}', 'Admin\CategoryController@edit');
    Route::post('/category/update/{id}', 'Admin\CategoryController@update');
    Route::post('/category/getByBrandId/{id}', 'Admin\CategoryController@getByBrandId');
    Route::get('/category/delete/{id}', 'Admin\CategoryController@destroy');

    /**
     * SubCategory Routes
     */

    Route::get('/subcategory', 'Admin\SubcategoryController@index');
    Route::get('/subcategory/add', 'Admin\SubcategoryController@add');
    Route::post('/subcategory/store', 'Admin\SubcategoryController@store');
    Route::post('/subcategory/getByCategoryId/{id}', 'Admin\SubcategoryController@getByCategoryId');
    Route::get('/subcategory/edit/{$id}', 'Admin\SubcategoryController@edit');

    /**
     * Product Routes
     */

    Route::get('/product', 'Admin\ProductController@index');
    Route::get('/product/add', 'Admin\ProductController@add');
    Route::post('/product/store', 'Admin\ProductController@store');
    Route::get('/product/edit/{id}', 'Admin\ProductController@edit');
    Route::post('/product/update/{id}', 'Admin\ProductController@update');
    Route::get('/product/delete/{id}', 'Admin\ProductController@destroy');
    Route::get('/product/show/{id}', 'Admin\ProductController@show');
    Route::get('/product/restore/{id}', 'Admin\ProductController@restore');

    /**
     * Size Controller Routes
     */

    Route::get('/sizes', 'Admin\SizeController@index');
    Route::post('/sizes/store', 'Admin\SizeController@store');
    Route::post('/sizes/destroy/{id}', 'Admin\SizeController@destroy');

});


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/prods', 'HomeController@prods');
Route::get('/category/{id}', 'HomeController@categoryProducts');
Route::get('/subcategory/{cat_id}/{subcat_id}', 'HomeController@subCategoryProducts');
Route::get('/product/{id}', 'HomeController@product');

//facebook

Route::get('auth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');
